<?php

$obj = new replacefile();
$obj->getfile("frontend");
$obj->read();

class replacefile {

	private $file_array = array();
	private $auto = 1;

	function getfile($dir) {
		$dir = $dir . "/";
		$file = scandir($dir);

		foreach ($file as $key => $value) {
			if (is_dir($dir . $value)) {
				if ($value != "." && $value != "..") {
					array_merge($this->file_array, self::getfile($dir . $value));
				}

			} else {
				array_push($this->file_array, $dir . $value);
			}
		}

		return $this->file_array;
	}

	function read() {
		foreach ($this->file_array as $key => $value) {
			echo "filename: $value " . self::checkBOM($value) . " <br>";
		}
	}

	function checkBOM($filename) {
		global $auto;
		$contents = file_get_contents($filename);
		$charset[1] = substr($contents, 0, 1);
		$charset[2] = substr($contents, 1, 1);
		$charset[3] = substr($contents, 2, 1);
		if (ord($charset[1]) == 239 && ord($charset[2]) == 187 && ord($charset[3]) == 191) {
			if ($auto == 1) {
				$rest = substr($contents, 3);
				rewrite($filename, $rest);
				return ("<font color=red>BOM found, automatically removed.</font>");
			} else {
				return ("<font color=red>BOM found.</font>");
			}
		} else {
			return ("BOM Not Found.");
		}

	}
	function rewrite($filename, $data) {
		$filenum = fopen($filename, "w");
		flock($filenum, LOCK_EX);
		fwrite($filenum, $data);
		fclose($filenum);
	}
}

?>